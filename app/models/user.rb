class User < ApplicationRecord
  has_many :users_account
  has_many :accounts, through: :users_account
end
