class Transaction < ApplicationRecord
  belongs_to :transaction_type
  belongs_to :transaction_status
  belongs_to :account
  belongs_to :user, :foreign_key => 'initiated_by_user'
  belongs_to :account, :foreign_key => 'target_account'
end
