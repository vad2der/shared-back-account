class Account < ApplicationRecord
  belongs_to :account_type
  belongs_to :withdraw_limit
  has_many :users_account
  has_many :users, through: :users_account
  has_many :transactions

  def create_transaction(withdraw_amount, transaction_action)
    transaction_id = generate_transaction_id()
    delay.generate_transaction(withdraw_amount, transaction_action) # to be executed from delayed queue
    return transaction_id
  end

  def generate_transaction(withdraw_amount, transaction_action)
    # will create a pending transaction, put a transaction into an external queue (messaging like SQS, etc.)
    # separate part of the app (or another app) would get transactions from queue and process them
  end
end
