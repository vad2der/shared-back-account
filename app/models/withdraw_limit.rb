class WithdrawLimit < ApplicationRecord
  belongs_to :time_scope
  has_many :accounts
end
