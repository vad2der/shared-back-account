class Api::V1::WithdrawFundsController < ApplicationController
  def create
    @account.create_transaction(withdraw_amount, transaction_action) # to be refactored in delayed jobs
    render json: {}, status: :created
  end
end
