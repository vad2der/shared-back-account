class Api::V1::ViewTransactionHistoryController < ApplicationController
  before_action :restrict_access
  before_action :set_account, only: [:show]
  
  def show
    render json: {transaction_history: @account.transactions}, status: :ok
  end

  private

  def view_transaction_history_params
    params.permit(:tokens, :format, :session, :auth_headers, :body, :account_id, :id)
  end

  def set_account
    @account = Account.where(id: view_transaction_history_params[:id])
                      .includes(:transactions)
                      .references(:transactions)
                      .first
    if @account.nil?
      render json: {error: "No such account found"}, status: :not_acceptable
    end
  end

  def restrict_access
    header_token = request.headers['X-Api-Key'] # here one can check temp user token and set the user
    @user = User.where(temp_token: header_token).first if header_token
    unless @user
      render json: { error: 'Unauthorized' }, status: :unauthorized
    end
  end
end
