class CreateWithdrawLimits < ActiveRecord::Migration[6.0]
  def change
    create_table :withdraw_limits do |t|
      t.decimal :limit, :precision => 8, :scale => 2
      t.references :time_scope, null: false, foreign_key: true

      t.timestamps
    end
  end
end
