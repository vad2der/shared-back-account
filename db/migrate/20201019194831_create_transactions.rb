class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.references :transaction_type, null: false, foreign_key: true
      t.references :transaction_status, null: false, foreign_key: true
      t.references :account, null: false, foreign_key: true
      t.decimal :balance_before, :precision => 8, :scale => 2
      t.decimal :amount, :precision => 8, :scale => 2
      t.decimal :fee, :precision => 8, :scale => 2, :default => 0
      t.decimal :balance_after, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
