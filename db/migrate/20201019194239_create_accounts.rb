class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.string :number
      t.references :account_type, null: false, foreign_key: true
      t.references :withdraw_limit, null: false, foreign_key: true
      t.decimal :balance, :precision => 8, :scale => 2
      t.decimal :balance_limit, :precision => 8, :scale => 2, :default => 0

      t.timestamps
    end
  end
end
