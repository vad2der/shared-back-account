class AddColumnsToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :initiated_by_user, :integer
    add_column :transactions, :target_account, :integer
  end
end
