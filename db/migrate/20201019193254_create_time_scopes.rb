class CreateTimeScopes < ActiveRecord::Migration[6.0]
  def change
    create_table :time_scopes do |t|
      t.string :name

      t.timestamps
    end
  end
end
