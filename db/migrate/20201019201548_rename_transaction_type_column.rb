class RenameTransactionTypeColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :transaction_types, :name, :action
  end
end
