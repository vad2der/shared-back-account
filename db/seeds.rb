TimeScope.create!(
  [
    {name: 'daily'}, {name: 'weekly'}, {name: 'monthly'}
  ]
)

AccountType.create!(
  [
    {name: 'personal', description: 'User personal account'},
    {name: 'shared', description: 'Multi-user shared account'}
  ]
)

UserType.create!(
  [
    {
      name: 'administrator',
      description: 'User can set limits on the withdraw and see history of an account on "normal" users'
    },
    {
      name: 'normal',
      description: 'User can use account within limitations, cannot chane those limitations'
    }
  ]
)
administrator_user_id = UserType.where(name: 'administrator').first.id

transaction_types = TransactionType.create!(
  [
    {action: 'add'},
    {action: 'withdraw'}
  ]
)

TransactionStatus.create!(
  [
    {name: 'pending'},
    {name: 'success'},
    {name: 'failed'},
    {name: 'cancelled'}
  ]
)

parent = User.create!(
  {
    first_name: "John",
    last_name: "Parent",
    dob: Date.parse('01-01-1990'),
    temp_token: 'sf8sfd8'
  }
)

child_1 = User.create!(
  {
    first_name: "Mike",
    last_name: "Child",
    dob: Date.parse('01-01-2000'),
    temp_token: 'sdf8ysd9'
  }
)

child_2 = User.create!(
  {
    first_name: "Colin",
    last_name: "Child",
    dob: Date.parse('01-01-2005'),
    temp_token: 'fgbg98ye4i'
  }
)

default_withdraw_limit = WithdrawLimit.create!(
  {
    time_scope_id: TimeScope.where(name: 'monthly').first.id,
    limit: 1000.00
  }
)

#Parent
personal_parent_account = Account.create!(
  {
    account_type_id: AccountType.where(name: 'personal').first.id,
    balance: 1000.00,
    number: 'P01',
    withdraw_limit_id: default_withdraw_limit.id
  }
)
#fill the join table
UsersAccount.create!(
  {
    user_id: parent.id,
    user_type_id: administrator_user_id,
    account_id: personal_parent_account.id
  }
)

#Child 1
personal_child_account_1 = Account.create!(
  {
    account_type_id: AccountType.where(name: 'personal').first.id,
    balance: 100.00,
    number: 'C01',
    withdraw_limit_id: default_withdraw_limit.id
  }
)
#fill the join table
UsersAccount.create!(
  {
    user_id: child_1.id,
    user_type_id: administrator_user_id,
    account_id: personal_child_account_1.id
  }
)

#Child 2
personal_child_account_2 = Account.create!(
  {
    account_type_id: AccountType.where(name: 'personal').first.id,
    balance: 200.00,
    number: 'C02',
    withdraw_limit_id: default_withdraw_limit.id
  }
)
#fill the join table
UsersAccount.create!(
  {
    user_id: child_2.id,
    user_type_id: administrator_user_id,
    account_id: personal_child_account_2.id
  }
)

#Shared account
shared_withdraw_limit =  WithdrawLimit.create!(
  {
    time_scope_id: TimeScope.where(name: 'monthly').last.id,
    limit: 500.00
  }
)

shared_account = Account.create!(
  {
    account_type_id: AccountType.where(name: 'shared').last.id,
    balance: 0.00,
    number: 'SH01',
    withdraw_limit_id: shared_withdraw_limit.id
  }
)

#fill the join table
UsersAccount.create!(
  [
    {
      user_id: parent.id,
      user_type_id: administrator_user_id,
      account_id: shared_account.id
    },
    {
      user_id: child_1.id,
      user_type_id: UserType.where(name: 'normal').first.id,
      account_id: shared_account.id
    }
  ]
)