# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_19_201846) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.string "number"
    t.bigint "account_type_id", null: false
    t.bigint "withdraw_limit_id", null: false
    t.decimal "balance"
    t.decimal "balance_limit"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_type_id"], name: "index_accounts_on_account_type_id"
    t.index ["withdraw_limit_id"], name: "index_accounts_on_withdraw_limit_id"
  end

  create_table "time_scopes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transaction_statuses", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transaction_types", force: :cascade do |t|
    t.string "action"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.bigint "transaction_type_id", null: false
    t.bigint "transaction_status_id", null: false
    t.bigint "account_id", null: false
    t.decimal "balance_before"
    t.decimal "amount"
    t.decimal "fee"
    t.decimal "balance_after"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "initiated_by_user"
    t.integer "target_account"
    t.index ["account_id"], name: "index_transactions_on_account_id"
    t.index ["transaction_status_id"], name: "index_transactions_on_transaction_status_id"
    t.index ["transaction_type_id"], name: "index_transactions_on_transaction_type_id"
  end

  create_table "user_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "dob"
    t.string "encripted_password"
    t.string "temp_token"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users_accounts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "account_id", null: false
    t.bigint "user_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_users_accounts_on_account_id"
    t.index ["user_id"], name: "index_users_accounts_on_user_id"
    t.index ["user_type_id"], name: "index_users_accounts_on_user_type_id"
  end

  create_table "withdraw_limits", force: :cascade do |t|
    t.decimal "limit"
    t.bigint "time_scope_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["time_scope_id"], name: "index_withdraw_limits_on_time_scope_id"
  end

  add_foreign_key "accounts", "account_types"
  add_foreign_key "accounts", "withdraw_limits"
  add_foreign_key "transactions", "accounts"
  add_foreign_key "transactions", "transaction_statuses"
  add_foreign_key "transactions", "transaction_types"
  add_foreign_key "users_accounts", "accounts"
  add_foreign_key "users_accounts", "user_types"
  add_foreign_key "users_accounts", "users"
  add_foreign_key "withdraw_limits", "time_scopes"
end
