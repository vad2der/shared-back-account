Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :place_withdraw_limit, only: [:create]
      resources :view_transaction_history_as_admin, only: [:show]
      resources :view_transaction_history, only: [:show]
      resources :view_available_balance, only: [:show]
      resources :withdraw_funds, only: [:create]
      resources :deposit_funds, only: [:create]
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
