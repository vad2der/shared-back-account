require 'test_helper'

class Api::V1::ViewTransactionHistoryAsAdminControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get api_v1_view_transaction_history_as_admin_show_url
    assert_response :success
  end

end
