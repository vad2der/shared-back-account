require 'test_helper'

class Api::V1::ViewTransactionHistoryControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get api_v1_view_transaction_history_show_url
    assert_response :success
  end

end
