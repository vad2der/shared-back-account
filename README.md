# 1 Installation
- clone the repository
- run `bundle` command to install dependancies
- have PostgreSQL running
- add env vars for DB_USERNAME and DB_PASSOWRD (`export DB_USERNAME=postgres` ...)
- create DB: `rails db:create`
- run migrations: `rails db:migrate`
- seed the DB: `rails db:seed`
- run `rails s` (it is available on localhost:3000 after that)

# 2 DB schema
DB schema presented here: [./erd.pdf](./erd.pdf)

# 3 Controllers
I have created requested controllers within api/v1

for example, this one 
app/controllers/api/v1/view_available_balance_controller.rb
has some authentication, parameter whitening, and simpliest json rendering

This one:
app/controllers/api/v1/deposit_funds_controller.rb (and app/models/account.rb)
has an approach I would take for transaction creation and processing

# 4 Disclaimer
It took defenitely more then 2 hours to think and write down everything here.